-- SUMMARY --

Do you need random URLs like http://example.com/kT6xl4a?
If it is, this module is for you.

This module provides random alpha numeric tokens such as 'o8jXwlsu.'
Main purpose of this token is to use as URLs. This type of URLs are
used on YouTube and many other sites.

For a full description of the module, visit the project page:
  http://drupal.org/sandbox/takah/1879162


-- REQUIREMENTS --

Token module. Pathauto module is not required, but we intend to use this
module with pathauto to generate random alpha numeric URLs automatically.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

No configuration at all. Just enable this module. You can use token such as
[random:alphanum:8] in Administration >> Configuration >> Search and
metadata >> URL aliases. The last parameter "8" is length. If you use
[random:alphanum:11], it generates a 11 length string.


-- FAQ --

Q: How long or short the generated tokens could be?

A. It's minimum 4 characters, maximum 36 characters.


-- CONTACT --

Current maintainer:
* Takahiko Horiuchi (Mikan-Shoin) - http://drupal.org/user/744036
